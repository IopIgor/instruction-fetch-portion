#ifndef INSTR_FETCH_HPP
#define INSTR_FETCH_HPP

#include "PC.hpp"
#include "Adder.hpp"
#include "instr_mem.hpp"

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(instr_fetch)
{
  static const unsigned bit_data = 32;
  

	sc_in<bool> clk;
  sc_in<bool> rst_n;
  sc_in<sc_bv<bit_data> > one;
  sc_out<sc_bv<bit_data> > instruction;

  sc_signal<sc_bv<bit_data> > pc_in_add_Y;
  sc_signal<sc_bv<bit_data> > pc_out_add_in_mem_in;

  reg PC;
  Adder add;
  instr_mem IM;

  SC_CTOR(instr_fetch) : PC("PC"), add("add"), IM("IM") 
  {
   	PC.clk(this->clk);   
   	PC.rst_n(this->rst_n); 
   	PC.in(this->pc_in_add_Y);
    PC.out(this->pc_out_add_in_mem_in);
   	add.A(this->pc_out_add_in_mem_in);
   	add.B(this->one);
    add.Y(this->pc_in_add_Y);
    IM.indirizzo(this->pc_out_add_in_mem_in); 
   	IM.istruzione(this->instruction);
  }
};

#endif
