#include <systemc.h>
#include <iostream>
#include "instr_fetch.hpp"

using namespace std;
using namespace sc_core;

const sc_time clk_semiperiod(1, SC_NS);
const unsigned bit_data = 32;
const unsigned MEM_SIZE = 14;
static sc_bv<bit_data> theor_instr[MEM_SIZE];

SC_MODULE(TestBench) 
{
 public:
  sc_signal<bool> clk;
  sc_signal<bool> rst_n;
  sc_signal<sc_bv<bit_data> > one;
  sc_signal<sc_bv<bit_data> > instruction;

  instr_fetch IF;
   
  SC_CTOR(TestBench) : IF("IF")
  {
    SC_THREAD(clk_thread);
    SC_THREAD(rst_thread);
    SC_THREAD(watcher_thread);
      sensitive << clk << rst_n;
    SC_THREAD(result_istr_update);
      sensitive << clk;

    IF.clk(this->clk);
    IF.rst_n(this->rst_n);
    IF.one(this->one);
    IF.instruction(this->instruction);

    theor_instr_init();
  }

  bool check() 
  {
    bool error = 0;
    for(unsigned i = 0; i < MEM_SIZE; i++)
    {
      if(theor_instr[i] != result_istr[i])
      {
        cout << "TEST FAILED!!\nTheoretical value: " << theor_instr[i]
             << "\nTest result:      " <<  result_istr[i] << endl << endl;
        error = 1;
      }
    }
    return error;
  }

  void watcher_thread()
  {
    while(!done)
    {
      wait();
      wait(1, SC_PS);
      if (clk.read() == 1)
        cout << "At time " << sc_time_stamp() << ":\n  rst is " << rst_n.read()
             << "\n  the input of the pc is " << IF.pc_in_add_Y.read().to_uint()
             << "\n  the output of the pc is " << IF.pc_out_add_in_mem_in.read().to_uint()
             << "\n  the instruction read is " << instruction.read() << endl;
    }
  }
  
 private:
  sc_bv<bit_data> result_istr[MEM_SIZE];
  bool done;

  void theor_instr_init()
  {
    for (unsigned i = 0; i < MEM_SIZE; i++) //memory intialized to 0
      theor_instr[i]=0;

    theor_instr[1] = "00100000001000010000000000000011"; 
    theor_instr[2] =  "00100000001000100000000000000101";
    theor_instr[3] = "10101100001000100000000000000000";
    theor_instr[4] = "10001100001000110000000000000000";
    theor_instr[5] = "00010000010000110000000000000001";
    theor_instr[6] = "00000000001000010010100000100000";
    theor_instr[7] = "00000000001000100010000000100000";
    theor_instr[8] = "00010000000000000000000000000001"; 
    theor_instr[9] = "00000000001000010010100000100000"; 
    theor_instr[10] = "00000000001000100011000000100000";
  }

  void result_istr_update()
  {
    unsigned i = 0;
    one.write(1);
    while(i < MEM_SIZE)
    {
      wait();
      if(clk.read() && rst_n)
      {
        result_istr[i] = instruction.read();
        i++;
      }
    }
  }

  void rst_thread()
  {
    rst_n.write(0);
    wait(2*clk_semiperiod);
    rst_n.write(1);
  }

  void clk_thread()
  {
    done = 0;
    unsigned i = 0;
    clk.write(0);
    while(i < MEM_SIZE)
    {
      wait(clk_semiperiod);
      if(clk.read() == 0) clk.write(1);
      else 
      {
        if(rst_n) i++;
        
        clk.write(0);
      }
    }
    done = 1;
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}

