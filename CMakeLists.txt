cmake_minimum_required (VERSION 2.6)

include_directories ("include")
include_directories ("include" "pc/include")
include_directories ("include" "adder/include")
include_directories ("include" "instruction-memory/include")

add_subdirectory ("pc")
add_subdirectory ("adder")
add_subdirectory ("instruction-memory")

add_executable (test_instr_fetch test/test_instr_fetch.cpp)

target_link_libraries (test_instr_fetch
	PC Adder instr_mem systemc)

enable_testing ()

add_test (instr_fetch test_instr_fetch)